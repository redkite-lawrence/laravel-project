@extends('layouts.master')

@section('content')

<h1>Contact TODOParrot</h1>

<ul>
	@foreach($errors->all() as $error)
		<li>{{$error}}</li>
	@endforeach
</ul>

@if(Session::has('message'))
	<div class="alert alert-info">
			{{ Session::get('message') }}
	</div>

@endif

{!! Form::open(array('route' => 'contact_store', 'class' => 'form')) !!}	{{-- default is POST, add 'method' => 'get' in array elements to make GET--}} {{-- 'class' is the CSS--}}

<div class="form-group">
	{!! Form::label('Your Name') !!}
	{!! Form::text('name', null,
		array('required',
			'class'=>'form-control',
			'placeholder'=>'Your name')) !!}
</div>


<div class="form-group">
	{!! Form::label('Your E-mail address') !!}
	{!! Form::text('email', null, 
		array('required', 
			'class'=>'form-control',
			'placeholder'=>'Your email address')) !!}
</div>

<div class="form-group">
	{!! Form::label('Your Message') !!}
	{!! Form::textarea('message', null,
		array('required',
			'class'=>'form-control',
			'placeholder'=>'Your message')) !!}
</div>

<div class="form-group">
	{!! Form::submit('Contact Us!',
		array('class'=>'btn btn-primary')) !!}
</div>

{!! Form::close() !!}

@endsection