@extends('layouts.master')

@section('content')
	<h2>This is a section</h2>	{{--this will only displayed if @yield('content') directive is on the layouts.master--}}
@endsection

{{-- SHORTCUT to display (define and yield) a section--}}
@section('content2')
	<h2>This is the second section</h2>
@show

{{-- THIS IS A COMMENT --}}

<?php //DETERMINING IF A VARIABLE IS SET ?>
<p>Welcome, {{$name or 'California'}} </p>

<a href="{{ URL::route('welcome.index') }}">Link</a>	<?php //this is a route alias when used?>
<br>

{{-- This is a comment in a view. --}}
<p>{{ $name }}</p><br>	<?php //$name comes from the welcome controller ?>
<p>You last visited {{$name}} on {{ $date }} </p>



<?php //ESCAPING DANGEROUS INPUT ?>
{{{ "My list <script>alert('spam spam spam!')</script>" }}}

<br><br>

<?php //LARAVEL'S WAY OF ITERATING OVER ARRAY?>
<ul>
{{--@foreach ($lists as $list) (USE THIS IF $lists can be empty--}}
	@forelse ($lists as $list)	<? //if may condition ?>
		<li>{{ $list }}</li>
	@empty
		<li>No item.</li>
	@endforelse
{{--@endforeach--}}
{{--FOREACH is more readable than FORELSE--}}


{{--Another version to iterate over arrays--}}
	@if (count($lists) > 1)
		<ul>
			@foreach ($lists as $list)
				<li>{{ $list }}</li>
			@endforeach
		</ul>
	@elseif (count($lists) == 1)
		<p>You have one list: {{ $lists[0] }}. </p>
	@else
		<p>You don't have any lists saved.</p>
	@endif
		</ul>




