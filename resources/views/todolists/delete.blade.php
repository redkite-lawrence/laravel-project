{!! Form::model($list, 
	array('route' => array('todolists.destroy', $list->id), 'method' => 'delete')) !!}

<div class="form-group">
	{!! Form::label('List Name:') !!}
	{!! Form::text('name', null,
		array('required', 'class' => 'form-control', 
		'placeholder' => 'San Juan Vacation')) !!}
</div>

<div class="form-group">
	{!! Form::label('List Description:') !!}
	{!! Form::textarea('description', null,
		array('required', 'class' => 'form-control', 
		'placeholder' => 'Things to do before vacation')) !!}
</div>

<div class="form-group">
	{!! Form::submit('Delete List', array('class' => 'btn btn-primary')) !!}
</div>

{!! Form::close() !!}