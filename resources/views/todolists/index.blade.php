@extends('layouts.master')

@section('content')
<h1>Lists</h1> 

@if ($lists->count() > 0)
	<table border=1>
			<tr>
				<td><b>List ID</b></td>
				<td><b>List Name</b></td>
				
				
				<td><b>Created at</b></td>
			</tr>

		@foreach ($lists as $list)

			<tr> 
				<td>{{$list->id}}</td>
				
				 <td><a href="{{ URL::route('todolists.show', ['id' => $list->id ]) }}"> {{$list->name}}</a>	  
				<td>{{$list->created_at}}</td>
			</tr>
		@endforeach

	</table>
@else
	<p>No lists found</p>
@endif

<b>{{$lists->count()}} records selected</b>

{!! $lists->render() !!}
<br>
<a href="{{ URL::route('todolists.create') }}">Create a list</a>
||
<a href="{{ URL::route('auth.logout') }}">Logout</a>
<!-- <a href="auth/logout">Logout</a> -->
@endsection

