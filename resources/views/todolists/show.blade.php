@extends('layouts.master')

@section('content')

<h1>{{ $list->name }}</h1>

<p>
<b>Created on:</b> {{ $list->created_at }}	<br />
<b>Last Modified:</b> {{ $list->updated_at }} <br/>
</p>

<p>
<b>Description:</b> {{ $list->description }}
</p>

{!! Form::model($list, 
	array('method' => 'get', 'route' => ['todolists.edit', $list->id], 'class' => 'form')) !!}
	<div class="form-group">
	{!! Form::submit('Edit todolist', array('class' => 'btn btn-primary' ))!!}
	</div>
{!! Form::close() !!}

<!-- DELETE MUST USE FORM, cant use <a> -->
{!! Form::model($list, 
	array('method' => 'delete', 'route' => ['todolists.destroy', $list->id], 'class' => 'form')) !!}
	<div class="form-group">
	{!! Form::submit('Delete todolist', array('class' => 'btn btn-primary' ))!!}
	</div>
{!! Form::close() !!}
	

<h2>Tasks</h2>
@if ($list->tasks->count() > 0)
	<ul>
	@foreach ($list->tasks as $task)
		<li>{{ $task->name }} - {{ $task->description }}</li>

	@endforeach
	</ul>
@else
	<p>You havent created any task for this TODOList</p>

@endif
	
	<a href="{{ URL::route('todolists.tasks.create', ['list_id' => $list->id]) }}" class='btn btn-primary'>Create a task</a>




@endsection
