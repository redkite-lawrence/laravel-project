@extends('layouts.master')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-3">
		</div>

		<div class="col-md-6">
			<div class="well">
			{!! Form::open( array('url' => 'auth/login', 'class' => 'form')) !!}
				<h3>Sign in to your Laravel App Account</h3>

				@if(count($errors) > 0)
					<div class="alert alert-danger">
						There were some problems signing into your account:
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif

				<div class="form-group">
					{!! Form::label('email', 'Your E-mail address') !!}
					{!! Form::text('email', null,
						array('class' => 'form-control', 'placeholder' => 'Email')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('password', 'Your Password') !!} <br>
					{!! Form::password('password', null,
						array('class' => 'form-control', 'placeholder' => 'Password')) !!}
					<br><a href="/password/email">Forgot your password?</a> <br>
				</div>

				<div class="form-group">
					<label>
						{!! Form::checkbox('remember', 'remember') !!} Remember me
					</label>
				</div>

				<div class="form-group">
					{!! Form::submit('Login', array('class' => 'btn btn-primary')) !!}
				</div>

				
				New User? <a href="/auth/register">Create an account</a>


				{!! Form::close() !!}
				</div>
			
			
		</div>
		<div class="col-md-3">
		</div>

	</div>
</div>

@endsection
