<!doctype HTML>

<html>
	<head>

		<meta charset="UTF-8">
		<title>Welcome to LARAVEL</title>

		<link rel="stylesheet"
		href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

		<script
			src="http://code.jquery.com/jquery-1.11.3.min.js">
		</script>
		<script
			src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js">
		</script>
	</head>

	<body>
		<div class="container">
			<div class="row">
					<div class="col-md-4">
					</div>
					<div class="col-md-4">
						<h1>LARAVEL Application</h1>
					</div>
					<div class="col-md-4">
					</div>
			</div>
			<div class="row">
				@yield('content') {{--name of the section that will be embedded in the master page--}}
			</div>
		</div>
	</body>

</html>