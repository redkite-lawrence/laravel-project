<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('profiles', function (Blueprint $table) {
        //     //
        // });

        Schema::create('profiles', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();        //must be unsigned() 
            $table->foreign('user_id')->references('id')->on('users'); //identifies 'user_id' as the foreign key

            // $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');  when a user is deleted, a profile will also be deleted
            $table->string('name');
            $table->string('telephone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function (Blueprint $table) {
            //
        });
    }
}
