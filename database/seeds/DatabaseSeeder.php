<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);     //name of the TableSeeder located in /seeds
        // DB::table('todolists')->delete();
        $this->call(TodolistTableSeeder::class);
        Model::reguard();
    }
}
