<?php

namespace todoparrot;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    
	#USING RELATIONSHIPS
    public function profile()	//note: this function must have the same name as the table that have relationship with this model
    {
    	// return $this->hasOne('todoparrot\Profile');	//this is uni directional, means you can only get a user info in profile but not vice versa,
    	//instead use this:
    	return $this->belongsTo('todoparrot\Profile');
    }
    //as a result, you can now use this:
    // public function getTelephoneFromProfile()
    // {
    // 	$user = User::find(1)->profile->telephone;
    // }


    #CREATING A ONE TO ONE RELATION
    $profile = new Profile();
    $profile->telephone = '123-456-7890';

    $user = User::find(212);
    $user->profile()->save($profile);

    #DELETING A ONE TO ONE RELATION
    $user = User::find(212);
    $user->profile()->delete();

    //as a result of using belongsTo above:
    $email = Profile::where('telephone', '123456789')->get()->first()->user->email;


}
