<?php

namespace todoparrot;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public function todolist()
    {
    	return $this->belongsTo('todoparrot\Todolist');
    }

    private $rules = [
    	'name' 			=> 'required',
    	'description' 	=> 'required',
    	'todolist_id'	=> 'required'
    ];

    protected $fillable = ['name', 'description', 'todolist_id'];

    public function validate()
    {
    	$v = \Validator::make($this->attribute, $this->rules);
    	if ($v->passes()) return true;
    	$this->errors = $v->messages();
    	return false;
    }

    #SCOPE - note of the naming convention
    public function scopeDone($query)
    {
    	return $query->where('done', 1);
    }
    //once the scope is defined, you can use Task::done()->get();
    //ex. $completedTasks = Tasks::done()->get();

    #DYNAMIC SCOPE
    // public function scopeDone($query, $flag)
    // {
    // 	return $query->where('done', $flag);
    // }
    //ex. $completedTasks = Task::done(true)->get();
    // $incompleteTasks = Task::done(false)->get();

}
