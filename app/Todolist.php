<?php

namespace todoparrot;

use Illuminate\Database\Eloquent\Model;

class Todolist extends Model
{
    public function tasks()
    {
        return $this->hasMany('todoparrot\Task');
    }

    public function categories()
    {
        return $this->belongsToMany('todoparrot\Category')->withTimestamps();
    }

    private $rules = [
    	'name' 			=> 'required',
    	'description' 	=> 'required',
    	'email' 		=> 'required:email|unique:users'
    ];

    protected $fillable = ['name', 'description'];	//these are the attributes that can be mass assigned

    protected $guarded = []; //these are the attributes that cannot be mass assigned

    


    public function validate()
    {
    	$v = \Validator::make($this->attribute, $this->rules);
    	if ($v->passes()) return true;
    	$this->errors = $v->messages();
    	return false;
    }






}
