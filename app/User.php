<?php

namespace todoparrot;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract   //must do  the implementation here
{
    use Authenticatable, CanResetPassword;  //these are TRAITS, allows MULTIPLE INHERITANCE

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'usersss'; //specifies the name of the model's underlying table

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];     //identify properties that should not be passed into JSON and arrays


}
