<?php

namespace todoparrot;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function todolists()
    {
    	return $this->belongsToMany('todoparrot\Todolists')->withTimestamps();
    }

    protected $fillable = ['name'];

}
