<?php

namespace todoparrot\Http\Middleware;

use Closure;

class RequestLogger
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \Log::info($request->ip());
        return $next($request);     //tells laravel to execute this middleware first before the request has been processed

        // $response = $next($request); //tells laravel to execute this middleware after the request has been processed
        // \Log::info($request->ip());
        // return $response;
    }
}
