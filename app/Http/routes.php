<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//HTTP VERBS
#GET
#POST
#PUT
#DELETE

Route::get('hello', function() {
	return 'hello world - using get';
});

Route::post('class', function(){
	return 'hello moto - using post';
});

### to respond in either GET or POST or other verbs
// Route::match(['get','post'], '/', function() {
// 	return 'hello motto';
// });

###TO RETURN VALUE FROM GET (route parameter)
Route::get('user/{id?}', function($id){ 	//the ?  makes the parameter optional
	return 'User_id: '.$id;
})
->where('id', '[0-9]+');	//w/ regular expression constraint
//[0-9] , [A-Za-z]

###return multiple values
Route::get('user/id/{id}/comment/{comment}', function($id, $comment) {
	return 'User_id: '.$id.'<br>'.'Comment: '.$comment;
})
->where(['id' => '[0-9]+', 'commentId' => '[A-Za-z]']);

#MIDDLEWARE USED
Route::get('user/age/{age}', ['middleware' => 'old', function($age) {	//'old' is the name registered to kernel.php
	return 'Age is '.$age;
}]);

#ROUTING VIEWS in CONTROLLER
Route::get('user/num/{id}', 'UserController@showProfile');	//with parameters passed
Route::get('welcome/index', 'WelcomeController@index');
// Route::get('iterationOverArray', 'WelcomeController@iterationOverArray');

// Route::controllers([
// 		'lists' => 'ListsController'
// 	]);


#CREATING ROUTE ALIASES aka NAMED ROUTES 	(para magamit yung controller_name.view_name sa Controller)
Route::get('welcome/index', ['as' => 'welcome.index', 'uses' => 'WelcomeController@index']);	
//pwede ng gamitin yung welcome.index sa view as link
//Example of using it:
// <a href="{{ URL::route('welcome.index')}}">Link</a>


Route::get('contact', 
	['as' => 'contact', 'uses' => 'AboutController@create']);
Route::post('contact', 
	['as' => 'contact_store', 'uses' => 'AboutController@store']);

#to route the entire controller
// Route::controllers([
// 	'lists' => 'ListsController'
// 	]);

#to tell laravel that it is a RESTFUL controller
// Route::resource('lists', 'TodoListController');	


Route::resource('todolists', 'TodoListController');
//NOTE: NO NEED NG GAWAN NG ROUTE ANG INDEX PAGE
//to access the homepage, type: http://localhost:8000/todolists/

#OPTIONAL PARAMETERS (using the ? operator) 	/show/{id}/{category?}

Route::get('todolists/show/{id}', 
	['as' => 'todolists.show', 'uses' => 'TodoListController@show']);	//ito ay papasok sa show($id) method ng controller
// Route::get('todolists/show/{id}/{category}', 'TodoListController@show');	//ito ay papasok sa show($id, $category) method ng controller

Route::get('todolists/create', 
	['as' => 'todolists.create', 'uses' => 'TodoListController@create']);

Route::get('todolists/edit/{id}',
	['as' => 'todolists.edit', 'uses' => 'TodoListController@edit']);
	

Route::get('todolists/{id}/delete',
	['as' => 'todolists.delete', 'uses' => 'TodoListController@delete']);

// Route::get('todolists/',
// 	['as' => 'todolists.many', 'uses' => 'TodoListController@many']);
#using it in links	<a href="{{ URL::route('todolists.delete', ['id' => '1']) }}"

// Route::get('todolists/create',
// 	['as' => 'todolists/create', 'uses' => 'TodoListController@create']);


// Route::get('todolists/create',  ['as' => 'todolists.create', 'uses' => 'ListsController@create']);

// Route::post('todolists/create', 
// 	['as' => 'todolists.store', 'uses' => 'ListsController@store']);


#WHEN YOU ROUTE A RESOURCE, these are the default routes
// GET 		/lists 			lists#index 			Display all TODO lists

// GET 		/lists/new 		lists#create 			Display an HTML form for
// 													creating a new TODO list

// POST 	/lists 			lists#store 			Create a new TODO list

// GET 		/lists/:id 		lists#show 				Display a specific TODO list

// GET 		/lists/:id/edit lists#edit 				Display an HTML form for editing
// 													an existing TODO list

// PUT 		/lists/:id 		lists#update 			Update an existing TODO list

// DELETE 	/lists/:id 		lists#destroy 			Delete an existing TODO list

Route::resource('tasks', 'TaskController');
Route::get('tasks/create/', 
	['as' => 'todolists.tasks.create', 'uses' => 'TaskController@create']);

// USING SESSIONS 
Route::get('yolo', function() {	//1st param - what is to be typed in the url
    // return view('welcome');		//display the view to be called
    return session('list_name');
});

// ACCOUNT REGISTRATION 
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');

Route::get('auth/logout', ['as' => 'auth.logout', 'uses' =>function(){
	Auth::logout();
	Session::flush();
	return Redirect::to('auth/login');
}]);