<?php

namespace todoparrot\Http\Controllers;

use Illuminate\Http\Request;

use todoparrot\Http\Requests;
use todoparrot\Http\Controllers\Controller;

use todoparrot\Http\Requests\TaskFormRequest;

use todoparrot\Todolist;
use todoparrot\Task;
use todoparrot\Category;

use DB;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        //
        
        $list_id = $request->get('list_id');

        // $id = session('list_id');
        // $name = session('list_name');

        $list = Todolist::findOrFail($list_id);

        // return view('todolists.tasks.create')->with(['id' => $list_id , 'name' => $name]);
        return view('todolists.tasks.create')->with(['list' => $list]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(TaskFormRequest $request)
    {
        //
        $task = new Task([
                'name'          =>  $request->get('name'),
                'description'   =>  $request->get('description'),
                'todolist_id'   =>  session('list_id')

            ]);

        $task->save();


        return \Redirect::route('todolists.show', array(session('list_id')));


    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
