<?php

namespace todoparrot\Http\Controllers;

use Illuminate\Http\Request;

use todoparrot\Http\Requests;
use todoparrot\Http\Controllers\Controller;
use todoparrot\Http\Requests\ContactFormRequest;

class AboutController extends Controller
{
    //
    public function create()	//responsible for displaying the VIEW
    {
    	return view('about.contact');	
    }

    public function store(ContactFormRequest $request)	//responsible for processing the submitted form data
    {
    	return \Redirect::route('contact')
    		->with('message', 'Thanks for contacting us!');
    }
}
